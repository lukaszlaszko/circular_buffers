[![Sources](https://img.shields.io/badge/bitbucket-sources-green.svg?style=flat)](https://bitbucket.org/lukaszlaszko/circular_buffers/src)
[![Pipelines](https://img.shields.io/badge/bitbucket-pipelines-blue.svg?style=flat)](https://bitbucket.org/lukaszlaszko/circular_buffers/addon/pipelines/home#!/)
[![Documentation](https://img.shields.io/badge/bitbucket-documentation-orange.svg?
style=flat)](http://lukaszlaszko.bitbucket.io/circular_buffers.git/master/)
##Table of contents

* [Overview](#markdown-header-overview)
* [Interface](#markdown-header-interface)
* [Performance](#markdown-header-performance)
* [Build](#markdown-header-build)
* [Conan package](#conan-package)
* [Troubleshooting](#troubleshooting)


---
##Overview

The library provides an easy to use implementation of different circular buffers. All buffers provided within the library can be classified along following properties:

* **Slot size**:
    + __Fixed size__ - element type is predefined during compilation and cannot be changed. Buffers like this have **_fixed_** keyword in name. 
    + __Variable size__ - element size is provided at runtime when individual elements are pushed. Each element may have different length. Buffers flike this have **_variable_** keyword in name. 
* **Number of publishing and consuming threads**:
    + __single-producer, single-consumer (spsc)__ - designed for configuration with single producing and single consuming thread. Buffers like this are prefixed with **spsc**.
    + __multiple-producers, single-consumer (mpsc)__ - designed for configuration with multiple producing producing and single consuming threads. This type of buffer is prefixed with **mpsc**. 

Following buffers are available:

* `usafe_fixed_circular_buffer` a simple container which can be used to store data only. It isn't thread safe and can't be used for inter-thread communication.

    **Usage:**

        #include <boost/concurrency.hpp>
        
        [...]

        using namespace boost::concurrency;

        usafe_fixed_circular_buffer<uint64_t> buffer(4);
        buffer.push([](uint64_t& element) { element = 1; });
        buffer.push([](uint64_t& element) { element = 2; });    
        buffer.push([](uint64_t& element) { element = 3; });
        buffer.push([](uint64_t& element) { element = 4; });

        [...]
    
        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        cout << "Count: " << buffer.count() << endl;
        [...]



* `spsc_fixed_circular_buffer` and `spsc_variable_circular_buffer` thread safe, single producer, single consumer ring buffers. It's intended to be used in unicast communication between two threads (and only two) where one of the threads is only producing and the other only consuming the data. This collection has the lowest letency and the highest throughput.

    **Usage:**

        #include <boost/concurrency.hpp>

        [...]

        using namespace boost::concurrency;

        // producer
        thread producer([&buffer, &start]()
        {
            start = timestamp::tick_count();
            for (uint64_t i = 0; i < LARGE_SAMPLE_COUNT; i++)
            {
                while (!buffer.push([i](Element& element)
                {
                    element.id = i;
                }));
            }
        });
    
        [...]
    
        // consumer
        thread consumer([&buffer, &end]()
        {
            uint64_t count = 0;
            while (count < LARGE_SAMPLE_COUNT)
            {
                buffer.consume([&count, &buffer](const Element& element)
                {
                    if (element.id != count++)
                        throw "element.id != count++";
 
                    return true;
                });
            }
            end = timestamp::tick_count();
        
        });
        [...]



* `mpsc_fixed_circular_buffer` and `mpsc_variable_circular_buffer` thread safe, multiple producer to single consumer ring buffers. It's intended to be used in situation when there are one or more threads are publishing and only one thread is consuming the data. In one-to-one communication it's advised to use `SpscCircularBuffer` as the letency and throughput characteristics are better.

    **Usage:**

        #include <boost/concurrency.hpp>

        [...]

        using namespace boost::concurrency;

        thread producer1([&buffer, &start1]()
        {
            for (uint64_t i = 0; i < LARGE_SAMPLE_COUNT; i++)
            {
                while (!buffer.push([i](Element& element)
                {
                    timespec time;
                    clock_gettime(CLOCK_REALTIME, &time);
                
                    element.producer = 1;
                    element.time = time;
                }));
            }
        });
    
        thread producer2([&buffer, &start2]()
        {
            for (uint64_t i = 0; i < LARGE_SAMPLE_COUNT; i++)
            {
                while (!buffer.push([i](Element& element)
                {
                    timespec time;
                    clock_gettime(CLOCK_REALTIME, &time);
                
                    element.producer = 2;
                    element.time = time;
                }));
            }
        });
    
        [...]
    
        thread consumer([&buffer, &end]()
        {
            while (true)
            {
                buffer.consume([](const Element& element)
                {
                    cout << element.producer << " : " << element.time << endl;

                    return true;
                });
            }
        });

##Interface

All the above circular buffers share the same interface:

* **Push** 
    This method is intended to be used by a publisher to publish the data onto the ring buffer. It's signature is following:
   
        // for fixed size element buffers:
        template <typename Callback>
        bool push(const Callback& callback);

        // for variable size element buffers:
        template <typename Callback>
        bool push(const Callback& callback, std::size_t size);

    The `Callback` has to be a lambda or function(method) pointer taking a reference to the ring buffer slot as a parameter. The method should not return anything. In order to push an element onto the ring buffer, client calls `Push` method, whenever it's possible a slot is claimed from the underlying collection and it's reference is returned back to the client through the callback. It's callback's responsibility to copy the data into the slot. `Push` returns `true` if publication was successful or `false` otherwise. Publication may fail if for instance ring buffer is full. In such case publisher should retry.

* **Consume**
    This method is intended to be used to consume data from the buffer. It's signature is following:
    
        template <typename Callback>
        bool consume(const Callback& callback);
    
    The `Callback` has to be a lambda or function(method) pointer taking a reference to the ring buffer slot as a parameter or byte buffer and it's length for variable size buffers. The method should not return any value. The method will be invoked once or multiple times within `Consume` method invocation and is intended to service reception of individual slots from the ring buffer. `Consume` method returns `true` to indicate successful consumption of at least one slot or `false` if there are no slots which could be consumed from the ring buffer. 
    
##Performance

Latency of the above buffers has been evaluated in micro-benchmarks. Below are the figures measured:

* `usafe_fixed_circular_buffer`

    *Publication*
    
    | Percentile | Latency (cycles) |
    |------------|------------------|
    | 50%        | 21               |
    | 70%        | 27               |
    | 90%        | 30               |
    | 95%        | 30               |
    
    > Ring buffer has been configured for **1mln** elements. Average push time measured **135 cycles**.

    The microbenchmark is available [here](https://bitbucket.org/lukaszlaszko/circular_buffers/src/HEAD/libConcurrent/benchmarks/circularBufferBenchmarks.cpp?at=master&fileviewer=file-view-default#circularBufferBenchmarks.cpp-53). 

* `spsc_fixed_circular_buffer`

    *Publish+Consume*
    
    | Percentile | Latency (cycles) |
    |------------|------------------|
    | 50%        |                  |
    | 70%        |                  |
    | 90%        |                  |
    | 95%        |                  |

    > Ring buffer has been configured for **8192 elements**, **1mln** events have been published. The average push-consume letency measured *~55 cycles*.

    The microbenchmark is available [here](https://bitbucket.org/lukaszlaszko/circular_buffers/src/HEAD/libConcurrent/benchmarks/spscCircularBufferBenchmarks.cpp?at=master&fileviewer=file-view-default#spscCircularBufferBenchmarks.cpp-69).

* `mpsc_fixed_circular_buffer`
    
    *Publish+Consume - 2 producers*
    
    | Percentile | Latency (cycles) |
    |------------|------------------|
    | 50%        |477               |
    | 70%        |594               |
    | 90%        |1836              |
    | 95%        |98694            |

    > Ring buffer has been configured for **8192 elements**, **1mln** events have been published. The average push-consume letency measured *~236 cycles*.

    The microbenchmark is available [here](https://bitbucket.org/lukaszlaszko/circular_buffers/src/HEAD/libConcurrent/benchmarks/mpscCircularBufferBenchmarks.cpp?at=master&fileviewer=file-view-default#mpscCircularBufferBenchmarks.cpp-190).


##Build

This a header-only library, thus no prior compilation is required. In order 
to use the headers in your project you only have to add `src/include` 
to your list of include directories used during compilation.   

In order to run unit tests and benchmarks a few more steps are required:

1. Make sure __CMake 3.2 or newer__ is available in your system. You can download cmake from [here](https://cmake.org/download/).
2. Clone the repository:

        $ git clone https://bitbucket.org/lukaszlaszko/circular_buffers.git
    
3. Create a build directory:

        $ cd circular_buffers
        $ mkdir bin

4. Run cmake from the bin directory to configure the project:

        $ cmake -DCMAKE_BUILD_TYPE=Release ..

    > Make, Ninja and Xcode are supported as code generators. Use `-G` switch to change cmake generator. 

5. Build using the underlying build system:

        $ cmake --build . --target all

6. Run unit tests

        $ ctest --verbose
        
## Conan package

[Conan](https://docs.conan.io/en/latest) is an opena source package manager for C/C++. [Bintray shadow](https://bintray.com/lukaszlaszko/shadow)
repository provides latest redistributable package with project artefacts. In order to use it in your cmake based project:

1. Add following block to root `CMakeLists.txt` of your project:

        if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
            include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
            conan_basic_setup()
        else()
            message(WARNING "The file conanbuildinfo.cmake doesn't exist, you have to run conan install first")
        endif()
    
    
2. Add `conanfile.txt` side by side with your top level `CMakeLists.txt`:
    
        [requires]
        circular_buffers/1.3@shadow/stable
        
        [generators]
        cmake   
    
4. Add `shadow` to your list of conan remotes:
    
        $ conan remote add shadow https://api.bintray.com/conan/lukaszlaszko/shadow     
    
3. Install conan dependencies into your cmake build directory:

        $ conan install . -if <build dir>   
    
    if for whatever reason installation of binary package fails, add `--build allocators` flag to the above. This will perform install the source package and compile all necessary modules.      
    
4. To link against libraries provided by the package, either add:

        target_link_libraries([..] ${CONAN_LIBS}) 
    
    for your target. Or specifically:
    
        target_link_libraries([..] ${CONAN_LIBS_CIRCULAR_BUFFERS})    
    
5. Reload cmake configuration.

## Troubleshooting

If during compilation with GCC > 5 error like this pops up:
```
undefined reference to `testing::internal::GetBoolAssertionFailureMessage[abi:cxx11](testing::AssertionResult const&, char const*, char const*, char const*)
```

make sure conan profile used for gtest installation is configured to use `libstdc++11`. This can be done either by setting **compiler.libcxx=libstdc++11** in `~/.conan/profiles/default` or by specifying it from command line.



Reference - https://docs.conan.io/en/latest/howtos/manage_gcc_abi.html  
