cmake_minimum_required(VERSION 3.2)
project(circular_buffers)

# register conan modules
if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup()
else()
    message(WARNING "The file conanbuildinfo.cmake doesn't exist, you have to run conan install first")
endif()

# register modules
include(CTest)
include(ExternalProject)

# common definitions
set(CMAKE_CXX_STANDARD 14)
add_definitions(-Wno-unknown-attributes)

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(RT_LIBRARY "rt")
    SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -pthread")
endif()

# find threads
find_package (Threads)

# find boost
set(BOOST_ROOT ${CONAN_BOOST_ROOT})
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME ON)
find_package(Boost 1.69.0 COMPONENTS system program_options)

if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
else()
    message(FATAL_ERROR "Boost couldn't be located, CMake will exit.")
endif()

#Setup CMake to run tests
enable_testing()

# add src to includes
include_directories(src/include)

# include subdirectories
add_subdirectory(src)
add_subdirectory(tests)
add_subdirectory(benchmarks)
add_subdirectory(samples)