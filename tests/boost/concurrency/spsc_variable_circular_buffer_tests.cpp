#include <gtest/gtest.h>

#include <boost/concurrency/spsc_variable_circular_buffer.hpp>
#include <boost/utils/random.hpp>

#include <cstring>
#include <unistd.h>
#include <vector>

using namespace std;
using namespace boost::utils;
using namespace boost::concurrency;


template <std::size_t ValidStart, std::size_t ValidEnd>
class test_mixin
{
public:
    size_t start_index_;
    size_t end_index_; 
    uint8_t* data_;
    
protected:
    static const bool MIXIN_ENABLED = true;
    
    void after_created(
            size_t& valid_start, 
            size_t& valid_end,
            uint8_t* data)
    {
        valid_start = ValidStart;
        valid_end = ValidEnd;
        
        start_index_ = valid_start;
        end_index_ = valid_end;
        data_ = data;
    }
    
    void before_push(size_t valid_start, size_t valid_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }
    
    void after_push(size_t valid_start, size_t valid_end, void* element) 
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }
    
    void before_consume(size_t valid_start, size_t valid_end) 
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }
    
    void after_consume(size_t valid_start, size_t valid_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }
};

TEST(spsc_variable_circular_buffer, push__start_lower_than_end__no_roll__positive)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<100, 200>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_TRUE(buffer.push([&data](uint8_t* dst)
                {
                    memcpy(dst, data.data(), data.size());
                }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 100);
    ASSERT_EQ(buffer.end_index_, 200 + data.size() + sizeof(size_t));
}

TEST(spsc_variable_circular_buffer, push__start_lower_than_end_roll__positive)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<200, 4095>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_TRUE(buffer.push([&data](uint8_t* dst)
                {
                    memcpy(dst, data.data(), data.size());
                }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 200);
    ASSERT_EQ(buffer.end_index_, data.size() + sizeof(size_t) - 1);
}

TEST(spsc_variable_circular_buffer, push__start_lower_than_end_roll__negative)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<100, 4095>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_FALSE(buffer.push([&data](uint8_t* dst)
                {
                    memcpy(dst, data.data(), data.size());
                }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 100);
    ASSERT_EQ(buffer.end_index_, 4095);
}

TEST(spsc_variable_circular_buffer, push__start_higher_than_end__positive)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<200, 50>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_TRUE(buffer.push([&data](uint8_t* dst)
                {
                    memcpy(dst, data.data(), data.size());
                }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 200);
    ASSERT_EQ(buffer.end_index_, 50 + data.size() + sizeof(size_t));
}

TEST(spsc_variable_circular_buffer, push__start_higher_than_end__nagative)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<207, 100>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_FALSE(buffer.push([&data](uint8_t* dst)
                             {
                                 memcpy(dst, data.data(), data.size());
                             }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 207);
    ASSERT_EQ(buffer.end_index_, 100);
}

TEST(spsc_variable_circular_buffer, push__start_lower_than_end_roll__nagative)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<100, 4095>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_FALSE(buffer.push([&data](uint8_t* dst)
                {
                    memcpy(dst, data.data(), data.size());
                }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 100);
    ASSERT_EQ(buffer.end_index_, 4095);
}

TEST(spsc_variable_circular_buffer, push__start_higher_than_end__exact_space)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<208, 100>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_FALSE(buffer.push([&data](uint8_t* dst)
                {
                    memcpy(dst, data.data(), data.size());
                }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 208);
    ASSERT_EQ(buffer.end_index_, 100);
}

TEST(spsc_variable_circular_buffer, push_consume__start_equal_to_end)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<100, 100>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_TRUE(buffer.push([&data](uint8_t* dst)
                             {
                                 memcpy(dst, data.data(), data.size());
                             }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 100);
    ASSERT_EQ(buffer.end_index_, 208);
    
    auto result = buffer.consume(
            [&data](const uint8_t* consumed, size_t length)
            {
                ASSERT_EQ(length, data.size());
                ASSERT_EQ(memcmp(data.data(), consumed, length), 0);
            });
    ASSERT_TRUE(result);
    
    ASSERT_EQ(buffer.start_index_, 208);
    ASSERT_EQ(buffer.end_index_, 208);
}

TEST(spsc_variable_circular_buffer, push_consume__roll)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<4090, 4090>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_TRUE(buffer.push([&data](uint8_t* dst)
                            {
                                memcpy(dst, data.data(), data.size());
                            }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 4090);
    ASSERT_EQ(buffer.end_index_, 102);
    
    auto result = buffer.consume(
            [&data](const uint8_t* consumed, size_t length)
            {
                ASSERT_EQ(length, data.size());
                ASSERT_EQ(memcmp(data.data(), consumed, length), 0);
            });
    ASSERT_TRUE(result);
    
    ASSERT_EQ(buffer.start_index_, 102);
    ASSERT_EQ(buffer.end_index_, 102);
}

TEST(spsc_variable_circular_buffer, push_consume__no_data_to_consume)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<102, 102>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    ASSERT_EQ(buffer.start_index_, 102);
    ASSERT_EQ(buffer.end_index_, 102);
    
    auto result = buffer.consume([](const uint8_t* consumed, size_t length) { });
    ASSERT_FALSE(result);
    
    ASSERT_EQ(buffer.start_index_, 102);
    ASSERT_EQ(buffer.end_index_, 102);
}

TEST(spsc_variable_circular_buffer, push_consume__fill_all)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<0, 0>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    vector<string> fixtures;
    
    // # 1
    fixtures.push_back(random::generate(capacity / 3)); // 1365
    
    {
        auto& data = fixtures.back();
        auto result = buffer.push([&data](uint8_t* dst)
        {
            memcpy(dst, data.data(), data.size());
        }, data.size());
        
        ASSERT_TRUE(result);
    }
 
    // #2
    fixtures.push_back(random::generate(capacity / 3)); // 1365
    
    {
        auto& data = fixtures.back();
        auto result = buffer.push([&data](uint8_t* dst)
        {
            memcpy(dst, data.data(), data.size());
        }, data.size());
        
        ASSERT_TRUE(result);
    }
    
    // #3
    fixtures.push_back(random::generate(capacity / 3)); // 1365
    
    {
        auto& data = fixtures.back();
        auto result = buffer.push([&data](uint8_t* dst)
        {
            memcpy(dst, data.data(), data.size());
        }, data.size());
        
        ASSERT_FALSE(result);
    }
    
    // consume
    auto count = 0ul;
    auto result = buffer.consume(
            [&fixtures, &count](const uint8_t* consumed, size_t length)
            {
                auto data = fixtures[count];
                ASSERT_EQ(length, data.size());
                ASSERT_EQ(memcmp(data.data(), consumed, length), 0);

                count++;
            });
    
    ASSERT_TRUE(result);
    ASSERT_EQ(count, 2);
    
    // retry pushing #3
    {
        auto& data = fixtures.back();
        auto result = buffer.push([&data](uint8_t* dst)
        {
            memcpy(dst, data.data(), data.size());
        }, data.size());
        
        ASSERT_TRUE(result);
    }
    
    result = buffer.consume(
            [&fixtures, &count](const uint8_t* consumed, size_t length) 
            {
                auto data = fixtures[count];
                ASSERT_EQ(length, data.size());
                ASSERT_EQ(memcmp(data.data(), consumed, length), 0);

                count++;
            });
    
    ASSERT_TRUE(result);
    ASSERT_EQ(count, 3);
}

TEST(spsc_variable_circular_buffer, push_consume__unsuccessful_consume)
{
    auto capacity = getpagesize();
    
    using Tester = test_mixin<100, 100>;
    spsc_variable_circular_buffer<Tester> buffer(capacity);
    
    auto data = random::generate(100);
    ASSERT_TRUE(buffer.push([&data](uint8_t* dst)
                             {
                                 memcpy(dst, data.data(), data.size());
                             }, data.size()));
    
    ASSERT_EQ(buffer.start_index_, 100);
    ASSERT_EQ(buffer.end_index_, 208);
    
    auto result = buffer.consume(
            [&data](const uint8_t* consumed, size_t length) { return false; });
    ASSERT_TRUE(result);
    
    ASSERT_EQ(buffer.start_index_, 100);
    ASSERT_EQ(buffer.end_index_, 208);
}
