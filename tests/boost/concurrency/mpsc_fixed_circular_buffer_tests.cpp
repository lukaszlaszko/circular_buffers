#include <gtest/gtest.h>

#include <boost/concurrency/mpsc_fixed_circular_buffer.hpp>

#include <atomic>
#include <array>
#include <functional>


using namespace std;
using namespace boost::concurrency;

template <std::size_t ValidStart, std::size_t ValidEnd, std::size_t ClaimedEnd=ValidEnd>
class test_mixin
{
public:
    using CommitFailedCallback = function<void(size_t&, size_t&, size_t&)>;
    
    size_t start_index_;
    size_t end_index_;
    size_t claimed_index_;
    
    void set_commit_failed_callback(CommitFailedCallback callback)
    {
        mOnCommitFailedCallback = callback;
    }
    
protected:
    static const bool MIXIN_ENABLED = true;
    
    void after_created(
            size_t& valid_start,
            size_t& valid_end,
            size_t& claimed_end)
    {
        valid_start = ValidStart;
        valid_end = ValidEnd;
        claimed_end = ClaimedEnd;
        
        start_index_ = valid_start;
        end_index_ = valid_end;
        claimed_index_ = claimed_end;
    }
    
    void before_push(
            size_t valid_start,
            size_t valid_end,
            size_t claimed_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
        claimed_index_ = claimed_end;
    }
    
    void after_claim(
            const size_t valid_start,
            const size_t valid_end,
            const size_t claimed_end)
    { }
    
    void failed_commit(
            size_t& valid_start,
            size_t& valid_end,
            size_t& claimed_end)
    {
        if (mOnCommitFailedCallback)
        {
            mOnCommitFailedCallback(valid_start, valid_end, claimed_end);
        }
    }
    
    void after_push(
            const size_t valid_start,
            const size_t valid_end,
            const size_t claimed_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
        claimed_index_ = claimed_end;
    }
    
    void before_consume(size_t valid_start, size_t valid_end) 
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }
    
    void after_consume(size_t valid_start, size_t valid_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }
    
private:
    CommitFailedCallback mOnCommitFailedCallback;
};

TEST(mpsc_fixed_circular_buffer, create_destroy__own_buffer)
{
    mpsc_fixed_circular_buffer<uint8_t> buffer(10);
}

TEST(mpsc_fixed_circular_buffer, push__single__start_lower_than_end)
{
    using Tester = test_mixin<3, 5>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3);
    EXPECT_EQ(buffer.end_index_, 5);
    EXPECT_EQ(buffer.claimed_index_, 5);
    
    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 3);
    EXPECT_EQ(buffer.end_index_, 6);
    EXPECT_EQ(buffer.claimed_index_, 6);
}

TEST(mpsc_fixed_circular_buffer, push__single__start_lower_than_end__end_last)
{
    using Tester = test_mixin<3, 7>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3);
    EXPECT_EQ(buffer.end_index_, 7);
    EXPECT_EQ(buffer.claimed_index_, 7);
    
    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 3);
    EXPECT_EQ(buffer.end_index_, 0);
    EXPECT_EQ(buffer.claimed_index_, 0);
}

TEST(mpsc_fixed_circular_buffer, push__single__start_lower_than_end__end_last_start_first)
{
    using Tester = test_mixin<0, 7>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 0);
    EXPECT_EQ(buffer.end_index_, 7);
    EXPECT_EQ(buffer.claimed_index_, 7);
    
    EXPECT_TRUE(!buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 0);
    EXPECT_EQ(buffer.end_index_, 7);
    EXPECT_EQ(buffer.claimed_index_, 7);
}

TEST(mpsc_fixed_circular_buffer, push__single__end_lower_than_start)
{
    using Tester = test_mixin<5, 3>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 5);
    EXPECT_EQ(buffer.end_index_, 3);
    EXPECT_EQ(buffer.claimed_index_, 3);
    
    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 5);
    EXPECT_EQ(buffer.end_index_, 4);
    EXPECT_EQ(buffer.claimed_index_, 4);
}

TEST(mpsc_fixed_circular_buffer, push__single__end_lower_than_start__start_next)
{
    using Tester = test_mixin<3, 2>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3);
    EXPECT_EQ(buffer.end_index_, 2);
    EXPECT_EQ(buffer.claimed_index_, 2);
    
    EXPECT_TRUE(!buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 3);
    EXPECT_EQ(buffer.end_index_, 2);
    EXPECT_EQ(buffer.claimed_index_, 2);
}

TEST(mpsc_fixed_circular_buffer, push__second__start_equal_end)
{
    using Tester = test_mixin<2, 5, 6>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    buffer.set_commit_failed_callback([](
            size_t& validStart,
            size_t& validEnd,
            size_t& claimeEnd)
    {
        // simulate commit from another thread
        validEnd += 1;
    });
    
    EXPECT_EQ(buffer.start_index_, 2);
    EXPECT_EQ(buffer.end_index_, 5);
    EXPECT_EQ(buffer.claimed_index_, 6);
    
    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 2);
    EXPECT_EQ(buffer.end_index_, 7);
    EXPECT_EQ(buffer.claimed_index_, 7);
}

TEST(mpsc_fixed_circular_buffer, consume__single__start_equal_to_end)
{
    using Tester = test_mixin<3, 3>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(!buffer.consume([](const uint64_t& element){ }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(mpsc_fixed_circular_buffer, consume__single__start_lower_than_end)
{
    using Tester = test_mixin<2, 3>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(mpsc_fixed_circular_buffer, consume__single__end_lower_than_start__before_roll)
{
    using Tester = test_mixin<7, 3>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 7ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(mpsc_fixed_circular_buffer, consume__single__end_first_start_last__before_roll)
{
    using Tester = test_mixin<7, 0>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 7ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));
    
    EXPECT_EQ(buffer.start_index_, 0ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
}

TEST(mpsc_fixed_circular_buffer, consume__unsuccessful)
{
    using Tester = test_mixin<2, 3>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ return false; }));
    
    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(mpsc_fixed_circular_buffer, count__start_equals_end)
{
    using Tester = test_mixin<3, 3>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 0ul);
}

TEST(mpsc_fixed_circular_buffer, count__start_lower_than_end)
{
    using Tester = test_mixin<1, 6>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 5ul);
}

TEST(mpsc_fixed_circular_buffer, count__end_lower_than_start)
{
    using Tester = test_mixin<4, 2>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 6ul);
}

TEST(MpscCircularBuffer, count__start_last_end_first)
{
    using Tester = test_mixin<7, 0>;
    mpsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 1ul);
}