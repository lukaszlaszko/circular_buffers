#include <gtest/gtest.h>

#include <boost/concurrency/variable_circular_buffer.hpp>

#include <unistd.h>

using namespace std;
using namespace boost::concurrency;


TEST(variable_circular_buffer, create_destroy__capacity_smaller_than_page_size)
{
    auto capacity = getpagesize() - 10;
    {
        variable_circular_buffer buffer(capacity);

        ASSERT_NE(buffer.capacity(), capacity);
        ASSERT_EQ(buffer.capacity(), getpagesize());
    }
}

TEST(variable_circular_buffer, create_destroy__capacity_equal_to_page_size)
{
    auto capacity = getpagesize();
    {
        variable_circular_buffer buffer(capacity);

        ASSERT_EQ(buffer.capacity(), capacity);
    }
}

