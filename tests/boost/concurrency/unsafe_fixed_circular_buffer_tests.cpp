#include <gtest/gtest.h>

#include <boost/concurrency/unsafe_fixed_circular_buffer.hpp>

#include <array>

using namespace std;
using namespace boost::concurrency;


template <std::size_t ValidStart, std::size_t ValidEnd>
class test_mixin
{
public:
    std::size_t start_index_;
    std::size_t end_index_;

protected:
    void after_created(std::size_t& valid_start, std::size_t& valid_end)
    {
        valid_start = ValidStart;
        valid_end = ValidEnd;

        start_index_ = valid_start;
        end_index_ = valid_end;
    }

    void before_push(std::size_t valid_start, std::size_t valid_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }

    void after_push(std::size_t valid_start, std::size_t valid_end, void* element)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }

    void before_consume(std::size_t valid_start, std::size_t valid_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }

    void after_consume(std::size_t valid_start, std::size_t valid_end)
    {
        start_index_ = valid_start;
        end_index_ = valid_end;
    }
};

TEST(unsafe_fixed_circular_buffer, create_destroy__own_buffer)
{
    unsafe_fixed_circular_buffer<uint8_t> buffer(10);
}

TEST(unsafe_fixed_circular_buffer, push__single__start_lower_than_end)
{
    using Tester = test_mixin<3, 5>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 5ul);

    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 6ul);
}

TEST(unsafe_fixed_circular_buffer, push__single__start_lower_than_end__one_behind_roll)
{
    using Tester = test_mixin<3, 7>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 7ul);

    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
}

TEST(unsafe_fixed_circular_buffer, push__single__end_lower_than_start)
{
    using Tester = test_mixin<3, 1>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 1ul);

    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 2ul);
}

TEST(unsafe_fixed_circular_buffer, push__single__end_lower_than_start__one_behind)
{
    using Tester = test_mixin<3, 2>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 2ul);

    EXPECT_TRUE(!buffer.push([](uint64_t& element) { element = 1; }));

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 2ul);
}

TEST(unsafe_fixed_circular_buffer, consume__single__start_equal_to_end)
{
    using Tester = test_mixin<3, 3>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);

    EXPECT_TRUE(!buffer.consume([](const uint64_t& element){ }));

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(unsafe_fixed_circular_buffer, consume__single__start_lower_than_end)
{
    using Tester = test_mixin<2, 3>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 3ul);

    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));

    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(unsafe_fixed_circular_buffer, consume__single__end_lower_than_start__before_roll)
{
    using Tester = test_mixin<7, 3>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 7ul);
    EXPECT_EQ(buffer.end_index_, 3ul);

    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));

    EXPECT_EQ(buffer.start_index_, 0ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(unsafe_fixed_circular_buffer, consume__single__end_first_start_last__before_roll)
{
    using Tester = test_mixin<7, 0>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.start_index_, 7ul);
    EXPECT_EQ(buffer.end_index_, 0ul);

    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));

    EXPECT_EQ(buffer.start_index_, 0ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
}

TEST(unsafe_fixed_circular_buffer, count__start_equals_end)
{
    using Tester = test_mixin<3, 3>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.count(), 0ul);
}

TEST(unsafe_fixed_circular_buffer, count__start_lower_than_end)
{
    using Tester = test_mixin<1, 6>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.count(), 5ul);
}

TEST(unsafe_fixed_circular_buffer, count__end_lower_than_start)
{
    using Tester = test_mixin<4, 2>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.count(), 6ul);
}

TEST(unsafe_fixed_circular_buffer, count__start_last_end_first)
{
    using Tester = test_mixin<7, 0>;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(8);

    EXPECT_EQ(buffer.count(), 1ul);
}

TEST(unsafe_fixed_circular_buffer, access_by_index__start_and_end_the_same)
{
    using Tester = test_mixin<4, 4>;

    array<uint64_t, 8> elements;
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(
            reinterpret_cast<uint8_t*>(elements.data()), 8);

    EXPECT_EQ(buffer.start_index_, 4ul);
    EXPECT_EQ(buffer.end_index_, 4ul);
    EXPECT_TRUE(buffer[0] == nullptr);
}

TEST(unsafe_fixed_circular_buffer, access_by_index__start_lower_than_end)
{
    using Tester = test_mixin<2, 4>;

    array<uint64_t, 8> elements { 88ul, 2ul, 3ul, 4ul, 5ul, 6ul, 7ul };
    unsafe_fixed_circular_buffer<uint64_t, Tester> buffer(
            reinterpret_cast<uint8_t*>(elements.data()), elements.size() * sizeof(uint64_t));

    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 4ul);
    ASSERT_EQ(buffer.count(), 2);
    ASSERT_TRUE(buffer[0] != nullptr);
    EXPECT_EQ(*buffer[0], elements[2]);
    ASSERT_TRUE(buffer[1] != nullptr);
    EXPECT_EQ(*buffer[1], elements[3]);
    EXPECT_TRUE(buffer[2] == nullptr);
}

