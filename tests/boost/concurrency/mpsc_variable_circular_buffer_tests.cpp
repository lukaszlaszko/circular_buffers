#include <gtest/gtest.h>

#include <boost/concurrency/mpsc_variable_circular_buffer.hpp>
#include <boost/utils/random.hpp>

#include <atomic>
#include <array>
#include <functional>

#include <unistd.h>

using namespace std;
using namespace boost::utils;
using namespace boost::concurrency;


template <std::size_t ValidStart, std::size_t ValidEnd, std::size_t ClaimedEnd=ValidEnd>
class test_mixin
{
public:
    using CommitFailedCallback = function<void(size_t&, size_t&, size_t&)>;

    size_t start_index_;
    size_t end_index_;
    size_t claimed_index_;
    uint8_t* data_;

    void set_commit_failed_callback(CommitFailedCallback callback)
    {
        on_commit_failed_callback_ = callback;
    }

protected:
    static const bool MIXIN_ENABLED = true;

    void after_created(
            size_t& validStart,
            size_t& validEnd,
            size_t& claimedEnd,
            uint8_t* data)
    {
        validStart = ValidStart;
        validEnd = ValidEnd;
        claimedEnd = ClaimedEnd;

        start_index_ = validStart;
        end_index_ = validEnd;
        claimed_index_ = claimedEnd;
        data_ = data;
    }

    void before_push(
            size_t validStart,
            size_t validEnd,
            size_t claimedEnd)
    {
        start_index_ = validStart;
        end_index_ = validEnd;
        claimed_index_ = claimedEnd;
    }

    void after_claim(
            const size_t validStart,
            const size_t validEnd,
            const size_t claimedEnd)
    { }

    void failed_commit(
            size_t& validStart,
            size_t& validEnd,
            size_t& claimedEnd)
    {
        if (on_commit_failed_callback_)
        {
            on_commit_failed_callback_(validStart, validEnd, claimedEnd);
        }
    }

    void after_push(
            const size_t validStart,
            const size_t validEnd,
            const size_t claimedEnd)
    {
        start_index_ = validStart;
        end_index_ = validEnd;
        claimed_index_ = claimedEnd;
    }

    void before_consume(size_t validStart, size_t validEnd)
    {
        start_index_ = validStart;
        end_index_ = validEnd;
    }

    void after_consume(size_t validStart, size_t validEnd)
    {
        start_index_ = validStart;
        end_index_ = validEnd;
    }

private:
    CommitFailedCallback on_commit_failed_callback_;
};

TEST(mpsc_variable_circular_buffer, create_destroy__own_buffer)
{
    mpsc_variable_circular_buffer<> buffer(10);

    ASSERT_EQ(buffer.capacity(), getpagesize());
}

TEST(mpsc_variable_circular_buffer, push__single__start_lower_than_end)
{
    using Tester = test_mixin<200, 500>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);

    EXPECT_EQ(buffer.start_index_, 200);
    EXPECT_EQ(buffer.end_index_, 500);
    EXPECT_EQ(buffer.claimed_index_, 500);

    auto data = random::generate(100ul);
    EXPECT_TRUE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 200);
    EXPECT_EQ(buffer.end_index_, 608);
    EXPECT_EQ(buffer.claimed_index_, 608);
}

TEST(mpsc_variable_circular_buffer, push__single__start_lower_than_end__end_last)
{
    using Tester = test_mixin<200, 4095>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);

    EXPECT_EQ(buffer.start_index_, 200);
    EXPECT_EQ(buffer.end_index_, 4095);
    EXPECT_EQ(buffer.claimed_index_, 4095);

    auto data = random::generate(100ul);
    EXPECT_TRUE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 200);
    EXPECT_EQ(buffer.end_index_, 107);
    EXPECT_EQ(buffer.claimed_index_, 107);
}

TEST(mpsc_variable_circular_buffer, push__single__start_lower_than_end__end_last_start_smaller_than_claim)
{
    using Tester = test_mixin<0, 4095>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);

    EXPECT_EQ(buffer.start_index_, 0);
    EXPECT_EQ(buffer.end_index_, 4095);
    EXPECT_EQ(buffer.claimed_index_, 4095);

    auto data = random::generate(100ul);
    EXPECT_FALSE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 0);
    EXPECT_EQ(buffer.end_index_, 4095);
    EXPECT_EQ(buffer.claimed_index_, 4095);
}

TEST(mpsc_variable_circular_buffer, push__single__end_lower_than_start)
{
    using Tester = test_mixin<500, 300>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);

    EXPECT_EQ(buffer.start_index_, 500);
    EXPECT_EQ(buffer.end_index_, 300);
    EXPECT_EQ(buffer.claimed_index_, 300);

    auto data = random::generate(100ul);
    EXPECT_TRUE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 500);
    EXPECT_EQ(buffer.end_index_, 408);
    EXPECT_EQ(buffer.claimed_index_, 408);
}

TEST(mpsc_variable_circular_buffer, push__single__end_lower_than_start__start_next)
{
    using Tester = test_mixin<308, 200>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);

    EXPECT_EQ(buffer.start_index_, 308);
    EXPECT_EQ(buffer.end_index_, 200);
    EXPECT_EQ(buffer.claimed_index_, 200);

    auto data = random::generate(100ul);
    EXPECT_FALSE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 308);
    EXPECT_EQ(buffer.end_index_, 200);
    EXPECT_EQ(buffer.claimed_index_, 200);
}

TEST(mpsc_variable_circular_buffer, push__second__start_equal_end)
{
    using Tester = test_mixin<200, 500, 608>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);
    buffer.set_commit_failed_callback([](
            size_t& valid_start,
            size_t& valid_end,
            size_t& claim_end)
    {
        // simulate commit from another thread
        valid_end += 108;
    });

    EXPECT_EQ(buffer.start_index_, 200);
    EXPECT_EQ(buffer.end_index_, 500);
    EXPECT_EQ(buffer.claimed_index_, 608);

    auto data = random::generate(100ul);
    EXPECT_TRUE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 200);
    EXPECT_EQ(buffer.end_index_, 716);
    EXPECT_EQ(buffer.claimed_index_, 716);
}

TEST(mpsc_variable_circular_buffer, consume__single__start_equal_to_end)
{
    using Tester = test_mixin<300, 300>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);

    EXPECT_EQ(buffer.start_index_, 300ul);
    EXPECT_EQ(buffer.end_index_, 300ul);

    EXPECT_FALSE(buffer.consume([](const uint8_t* src, size_t length) { }));

    EXPECT_EQ(buffer.start_index_, 300ul);
    EXPECT_EQ(buffer.end_index_, 300ul);
}

TEST(mpsc_variable_circular_buffer, consume__single__start_lower_than_end)
{
    using Tester = test_mixin<200, 200>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);
    
    auto data = random::generate(100ul);
    EXPECT_TRUE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 200ul);
    EXPECT_EQ(buffer.end_index_, 308ul);

    EXPECT_TRUE(buffer.consume([](const uint8_t* src, size_t length) { }));

    EXPECT_EQ(buffer.start_index_, 308ul);
    EXPECT_EQ(buffer.end_index_, 308ul);
}

TEST(mpsc_variable_circular_buffer, consume__single__end_lower_than_start__before_roll)
{
    using Tester = test_mixin<700, 300>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);
    *(reinterpret_cast<size_t*>(buffer.data_ + 700)) = 3688ul;

    EXPECT_EQ(buffer.start_index_, 700ul);
    EXPECT_EQ(buffer.end_index_, 300ul);

    EXPECT_TRUE(buffer.consume([](const uint8_t* src, size_t length){ }));

    EXPECT_EQ(buffer.start_index_, 300ul);
    EXPECT_EQ(buffer.end_index_, 300ul);
}

TEST(mpsc_variable_circular_buffer, consume__single__zero_size__roll_after)
{
    using Tester = test_mixin<4088, 0>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);

    EXPECT_EQ(buffer.start_index_, 4088ul);
    EXPECT_EQ(buffer.end_index_, 0ul);

    EXPECT_TRUE(buffer.consume([](const uint8_t* src, size_t length){ }));

    EXPECT_EQ(buffer.start_index_, 0ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
}

TEST(mpsc_variable_circular_buffer, consume__unsuccessful)
{
    using Tester = test_mixin<200, 200>;
    mpsc_variable_circular_buffer<Tester> buffer(4096);
    
    auto data = random::generate(100ul);
    EXPECT_TRUE(buffer.push([&data](uint8_t* dst)
    {
        memcpy(dst, data.data(), data.size());
    }, data.size()));

    EXPECT_EQ(buffer.start_index_, 200ul);
    EXPECT_EQ(buffer.end_index_, 308ul);

    EXPECT_TRUE(buffer.consume([](const uint8_t* src, size_t length) { return false; }));

    EXPECT_EQ(buffer.start_index_, 200ul);
    EXPECT_EQ(buffer.end_index_, 308ul);
}

