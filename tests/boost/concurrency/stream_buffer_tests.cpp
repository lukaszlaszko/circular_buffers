#include <boost/concurrency/stream_buffer.hpp>

#include <gtest/gtest.h>

#include <string>

using namespace std;
using namespace boost::concurrency;


TEST(stream_buffer, create)
{
    stream_buffer buffer(2048);
    ASSERT_EQ(buffer.capacity(), 4096ul);
}

TEST(stream_buffer, write__no_wrap)
{
    stream_buffer buffer(2048);
    auto read_ref = buffer.read_buf();
    auto write_ref = buffer.write_buf();

    ASSERT_EQ(read_ref.length(), 0ul);
    ASSERT_EQ(write_ref.length(), buffer.capacity());

    string data(512, 'x');
    ::memccpy(write_ref.as_pointer<void*>(), data.data(), write_ref.length(), data.length());
    buffer.write_commit(data.length());

    auto new_read_ref = buffer.read_buf();
    auto new_write_ref = buffer.write_buf();
    ASSERT_EQ(new_read_ref.length(), data.length());
    ASSERT_EQ(new_write_ref.as_pointer<char*>(), write_ref.as_pointer<char*>() + data.length());
}

TEST(stream_buffer, write__read__with_wrap)
{
    stream_buffer buffer(2048);
    auto read_ref_1 = buffer.read_buf();
    auto write_ref_1 = buffer.write_buf();

    ASSERT_EQ(read_ref_1.length(), 0ul);
    ASSERT_EQ(write_ref_1.length(), buffer.capacity());

    buffer.write_commit(4090ul);

    auto read_ref_2 = buffer.read_buf();
    auto write_ref_2 = buffer.write_buf();
    ASSERT_EQ(read_ref_2.length(), 4090ul);
    ASSERT_EQ(write_ref_2.as_pointer<char*>(), write_ref_1.as_pointer<char*>() + 4090ul);

    buffer.read_commit(1000ul);

    auto read_ref_3 = buffer.read_buf();
    auto write_ref_3 = buffer.write_buf();
    ASSERT_EQ(read_ref_3.as_pointer<char*>(), read_ref_2.as_pointer<char*>() + 1000ul);
    ASSERT_EQ(write_ref_3.length(), 1006ul);

    buffer.write_commit(16ul);

    auto read_ref_4 = buffer.read_buf();
    auto write_ref_4 = buffer.write_buf();
    ASSERT_EQ(write_ref_4.as_pointer<char*>(), write_ref_1.as_pointer<char*>() + 10ul);

    buffer.read_commit(3100);
    auto read_ref_5 = buffer.read_buf();
    auto write_ref_5 = buffer.write_buf();
    ASSERT_EQ(read_ref_5.as_pointer<char*>(), read_ref_1.as_pointer<char*>() + 4ul);
}
