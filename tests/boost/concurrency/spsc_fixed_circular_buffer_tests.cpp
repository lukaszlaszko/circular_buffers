#include <gtest/gtest.h>

#include <boost/concurrency/spsc_fixed_circular_buffer.hpp>

#include <atomic>
#include <array>


using namespace std;
using namespace boost::concurrency;

template <std::size_t ValidStart, std::size_t ValidEnd>
class test_mixin
{
public:
    std::size_t start_index_;
    std::size_t end_index_;    
    
protected:
    static const bool MIXIN_ENABLED = true;
    
    void after_created(std::size_t& validStart, std::size_t& validEnd)
    {
        validStart = ValidStart;
        validEnd = ValidEnd;
        
        start_index_ = validStart;
        end_index_ = validEnd;
    }
    
    void before_push(std::size_t validStart, std::size_t validEnd)
    {
        start_index_ = validStart;
        end_index_ = validEnd;
    }
    
    void after_push(std::size_t validStart, std::size_t validEnd, void* element) 
    {
        start_index_ = validStart;
        end_index_ = validEnd;
    }
    
    void before_consume(std::size_t validStart, std::size_t validEnd) 
    {
        start_index_ = validStart;
        end_index_ = validEnd;
    }
    
    void after_consume(std::size_t validStart, std::size_t validEnd)
    {
        start_index_ = validStart;
        end_index_ = validEnd;
    }
};

TEST(spsc_fixed_circular_buffer, create_destroy__own_buffer)
{
    spsc_fixed_circular_buffer<uint8_t> buffer(10);
}

TEST(spsc_fixed_circular_buffer, push__single__start_lower_than_end)
{
    using Tester = test_mixin<3, 5>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 5ul);
    
    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 6ul);
}

TEST(spsc_fixed_circular_buffer, push__single__start_lower_than_end__one_behind_roll)
{
    using Tester = test_mixin<3, 7>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 7ul);
    
    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
}

TEST(spsc_fixed_circular_buffer, push__single__end_lower_than_start)
{
    using Tester = test_mixin<3, 1>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 1ul);
    
    EXPECT_TRUE(buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 2ul);
}

TEST(spsc_fixed_circular_buffer, push__single__end_lower_than_start__one_behind)
{
    using Tester = test_mixin<3, 2>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 2ul);
    
    EXPECT_TRUE(!buffer.push([](uint64_t& element) { element = 1; }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 2ul);
}

TEST(spsc_fixed_circular_buffer, consume__single__start_equal_to_end)
{
    using Tester = test_mixin<3, 3>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(!buffer.consume([](const uint64_t& element){ return true; }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(spsc_fixed_circular_buffer, consume__single__start_lower_than_end)
{
    using Tester = test_mixin<2, 3>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(spsc_fixed_circular_buffer, consume__single__end_lower_than_start__before_roll)
{
    using Tester = test_mixin<7, 3>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 7ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));
    
    EXPECT_EQ(buffer.start_index_, 3ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(spsc_fixed_circular_buffer, consume__single__end_first_start_last__before_roll)
{
    using Tester = test_mixin<7, 0>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 7ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ }));
    
    EXPECT_EQ(buffer.start_index_, 0ul);
    EXPECT_EQ(buffer.end_index_, 0ul);
}

TEST(spsc_fixed_circular_buffer, consume__unsuccessful)
{
    using Tester = test_mixin<2, 3>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
    
    EXPECT_TRUE(buffer.consume([](const uint64_t& element){ return false; }));
    
    EXPECT_EQ(buffer.start_index_, 2ul);
    EXPECT_EQ(buffer.end_index_, 3ul);
}

TEST(spsc_fixed_circular_buffer, count__start_equals_end)
{
    using Tester = test_mixin<3, 3>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 0ul);
}

TEST(spsc_fixed_circular_buffer, count__start_lower_than_end)
{
    using Tester = test_mixin<1, 6>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 5ul);
}

TEST(spsc_fixed_circular_buffer, count__end_lower_than_start)
{
    using Tester = test_mixin<4, 2>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 6ul);
}

TEST(spsc_fixed_circular_buffer, count__start_last_end_first)
{
    using Tester = test_mixin<7, 0>;
    spsc_fixed_circular_buffer<uint64_t, Tester> buffer(8);
    
    EXPECT_EQ(buffer.count(), 1ul);
}