#include <boost/concurrency/unsafe_fixed_circular_buffer.hpp>

#include <array>
#include <iostream>

using namespace std;
using namespace boost::concurrency;

class BufferLogger
{
protected:
    void after_created(size_t& validStart, size_t& validEnd)
    {
        cout << "DEBUG: Buffer created. Start=" << validStart 
             << " End=" << validEnd 
             << endl;
    }
    
    void before_push(size_t validStart, size_t validEnd)
    {
        cout << "DEBUG: Before push. Start=" << validStart 
             << " End=" << validEnd 
             << endl;
    }
    
    void after_push(size_t validStart, size_t validEnd, uint64_t* pushed)
    {
        cout << "DEBUG: After push. Start=" << validStart 
             << " End=" << validEnd 
             << endl;
    }
    
    void before_consume(size_t validStart, size_t validEnd)
    {
        cout << "DEBUG: Before consume. Start=" << validStart 
             << " End=" << validEnd 
             << endl;
    }
    
    void after_consume(size_t validStart, size_t validEnd)
    {
        cout << "DEBUG: After consume. Start=" << validStart 
             << " End=" << validEnd 
             << endl;
    }
};

int main(int argc, char** argv)
{
    {
        unsafe_fixed_circular_buffer<uint64_t, BufferLogger> buffer(4);
        buffer.push([](uint64_t& element) { element = 1; });
        buffer.push([](uint64_t& element) { element = 2; });    
        buffer.push([](uint64_t& element) { element = 3; });
        buffer.push([](uint64_t& element) { element = 4; });

        for (auto i = 0; i < buffer.count(); i++)
            cout << *buffer[i] << " ";
        cout << endl;

        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        cout << "Count: " << buffer.count() << endl;

        buffer.push([](uint64_t& element) { element = 5; });
        buffer.push([](uint64_t& element) { element = 6; });
        cout << "Count: " << buffer.count() << endl;

        for (auto i = 0; i < buffer.count(); i++)
            cout << *buffer[i] << " ";
        cout << endl;

        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        cout << "Count: " << buffer.count() << endl;

        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
        buffer.consume([](const uint64_t& element) { cout << "Consumed: " << element << endl; });
    }
    
    return 0;
}
