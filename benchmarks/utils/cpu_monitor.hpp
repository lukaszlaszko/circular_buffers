#pragma once

#include <iostream>
#include <memory>

#ifdef INTEL_PCM_FOUND

#include <cpucounters.h>


class cpu_monitor
{
public:
    cpu_monitor();
    ~cpu_monitor();
    
    cpu_monitor(const cpu_monitor&) = delete;
    cpu_monitor(cpu_monitor&&) = delete;
    cpu_monitor& operator=(const cpu_monitor&) = delete;
    cpu_monitor& operator=(cpu_monitor&&) = delete;
    
    void read(std::ostream& os);
    void read_and_reset(std::ostream& os);
    
private:
    PCM* pcm_;
    SystemCounterState before_;
};

#else

class cpu_monitor
{
public:
    cpu_monitor();
    ~cpu_monitor();
    
    cpu_monitor(const cpu_monitor&) = delete;
    cpu_monitor(cpu_monitor&&) = delete;
    cpu_monitor& operator=(const cpu_monitor&) = delete;
    cpu_monitor& operator=(cpu_monitor&&) = delete;
    
    void read(std::ostream& os);
    void read_and_reset(std::ostream& os);
};

#endif

#include "cpu_monitor.ipp"

