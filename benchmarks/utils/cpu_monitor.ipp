#pragma once

#ifdef INTEL_PCM_FOUND

#include <iostream>
#include <stdexcept>

#include <cpucounters.h>

#include "cpu_monitor.hpp"


inline cpu_monitor::cpu_monitor()
{
    pcm_ = PCM::getInstance();
    pcm_->resetPMU();
    
    if (pcm_->program() != PCM::Success) 
        throw std::logic_error("Intel PCM can't be started!");
    
    before_ = pcm_->getSystemCounterState();
}

inline cpu_monitor::~cpu_monitor()
{
    pcm_->cleanup();
}

inline void cpu_monitor::read(std::ostream& os)
{
    auto after = pcm_->getSystemCounterState();
    
    os << "IPC=" << getIPC(before_, after) << std::endl;
    os << "Hit Ratio L2=" << getL2CacheHitRatio(before_, after) << std::endl;
    os << "Hit Ratio L3=" << getL3CacheHitRatio(before_, after) << std::endl;
    os << "Cycles lost due to L2 miss=" << getCyclesLostDueL2CacheMisses(before_, after) << std::endl;
    os << "Cycles lost due to L2 miss=" << getCyclesLostDueL3CacheMisses(before_, after) << std::endl;
}

inline void cpu_monitor::read_and_reset(std::ostream& os)
{
    read(os);
    before_ = pcm_->getSystemCounterState();
}

#else

cpu_monitor::cpu_monitor()
{  
    std::cout << "cpu monitor is disabled!" << std::endl;
}

cpu_monitor::~cpu_monitor()
{    
}

void cpu_monitor::read(std::ostream& os)
{
}

void cpu_monitor::read_and_reset(std::ostream& os)
{
}

#endif
