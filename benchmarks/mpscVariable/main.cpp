#include <cpu_monitor.hpp>

#include <boost/concurrency/mpsc_variable_circular_buffer.hpp>
#include <boost/concurrency/current_thread.hpp>
#include <boost/utils/random.hpp>
#include <boost/utils/timestamp.hpp>

#include <boost/program_options.hpp>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <list>
#include <thread>
#include <vector>

using namespace std;
using namespace boost::utils;
using namespace boost::concurrency;

static constexpr auto HELP_PARAMETER = "help,h";
static constexpr auto BUFFER_SIZE_PARAMETER = "buffer-size,b";
static constexpr auto PUBLISHED_SIZE_PARAMETER = "published-size,s";
static constexpr auto BURST_SIZE_PARAMETER = "burst-size,i";
static constexpr auto PRODUCERS_CORES_PARAMETER = "producers-cores,p";
static constexpr auto CONSUMER_CORE_PARAMETER = "consumer-core,c";

static constexpr auto DEFAULT_PUBLISHED_SIZE = 30;
static constexpr auto DEFAULT_BURST_SIZE = 100000000; 

static constexpr auto SUCCESS = 0;
static constexpr auto ERROR_IN_COMMAND_LINE = -1;
static constexpr auto ERROR_UNHANDLED_EXCEPTION = -2;


void run(
        bool warmup, 
        size_t buffer_size, 
        size_t burst, 
        string& data,
        vector<uint8_t>& producers_cores,
        size_t consumer_core)
{
    uint64_t start;
    uint64_t stop;
    
    mpsc_variable_circular_buffer<> buffer(buffer_size);
    thread consumer([&stop, &buffer, consumer_core, burst]()
    {
        current_thread::affinity_mask mask;
        mask.set(consumer_core);
        current_thread::set_affinity(mask);

        cpu_monitor monitor;
        
        auto count = 0ul;
        while (count < burst)
        {
            buffer.consume([&count](const uint8_t* slot, size_t length) 
            { 
                count++;
                return true;
            });
        }
        
        stop = timestamp::tick_count();
        
        monitor.read(cout);
    });

    list<thread> producers;
    for (auto i = 0ul; i < producers_cores.size(); i++)
    {
        auto& producer_core = producers_cores[i];
        producers.emplace_back(
                [&start, i, &burst, &buffer, &data, &producer_core]()
                {
                    if (i == 0ul)
                        start = timestamp::tick_count();
                    
                    current_thread::affinity_mask mask;
                    mask.set(producer_core);
                    current_thread::set_affinity(mask);

                    for (auto i = 0ul; i < burst; i++)
                    {
                        while(!buffer.push([&data](uint8_t* slot)
                        {
                            memcpy(slot, data.data(), data.size());
                        }, data.size()));
                    }
                });
    }

    consumer.join();
    for (auto& producer : producers)
        producer.join();
    
    if (!warmup)
    {
        cout << endl;
        cout << "Number of cycles: " << stop - start << endl;
        cout << "Data in bytes: " << data.size() * burst << endl;
        cout << "Cycles/Block: " << (stop - start) / burst << endl;
    }
}

int main(int argc, char** argv) 
{
    try
    {
        bool help;
        size_t buffer_size;
        size_t published_size;
        size_t burst_size;
        vector<uint8_t> producers_cores;
        size_t consumer_core;

        boost::program_options::options_description description("Options");
        description.add_options()
            (HELP_PARAMETER, boost::program_options::bool_switch(&help), "Print help messages")
            (BUFFER_SIZE_PARAMETER, boost::program_options::value<size_t>(&buffer_size)->default_value(getpagesize()), "Ring buffer size. It's always rounded to the closer multiplication of page size.")
            (PUBLISHED_SIZE_PARAMETER, boost::program_options::value<size_t>(&published_size)->default_value(DEFAULT_PUBLISHED_SIZE), "Number of bytes to publish")
            (BURST_SIZE_PARAMETER, boost::program_options::value<size_t>(&burst_size)->default_value(DEFAULT_BURST_SIZE), "Number of iterations")
            (PRODUCERS_CORES_PARAMETER, boost::program_options::value<vector<uint8_t>>(&producers_cores)->multitoken(), "Producer threads core id assignment (0-number of cores in the system)")
            (CONSUMER_CORE_PARAMETER, boost::program_options::value<size_t>(&consumer_core)->required(), "Consumer thread core id (0-number of cores in the system)");

        boost::program_options::variables_map variables;
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_command_line(argc, argv, description),
                    variables);

            if (help)
            {
                cout << "SPSC Benchmark:" << endl;
                cout << description << endl;

                return SUCCESS;
            }

            boost::program_options::notify(variables);
        }
        catch(boost::program_options::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << description << endl;

            return ERROR_IN_COMMAND_LINE;
        }

        auto data = random::generate(published_size);
        
        cout << "Warmup..." << flush;
        run(true, buffer_size, burst_size, data, producers_cores, consumer_core);
        cout << "done" << endl;
        
        cout << "Running..." << flush;
        run(false, buffer_size, burst_size, data, producers_cores, consumer_core);
        cout << "done" << endl;

        return SUCCESS;
    }
    catch(exception& e)
    {
        cerr << "Unhandled Exception reached the top of main: ";
        cerr << e.what() << ", application will now exit" << endl;

        return ERROR_UNHANDLED_EXCEPTION;
    }
}

