# find Intel PCM
if (INTEL_PCM_ROOT)
    message(STATUS "Found Intel PCM")
    set(INTEL_PCM_INCLUDE_DIRS "${INTEL_PCM_ROOT}")
    set(INTEL_PCM_LIBRARIES "${INTEL_PCM_ROOT}/intelpcm.so/libintelpcm.so")
    add_definitions(-DINTEL_PCM_FOUND)
endif()

include_directories(${GTEST_INCLUDE_DIRS})
include_directories(${INTEL_PCM_INCLUDE_DIRS})
include_directories(utils)

add_executable(benchmarks
        unsafe_fixed_circular_buffer_benchmarks.cpp
        spsc_fixed_circular_buffer_benchmarks.cpp
        mpsc_fixed_circular_buffer_benchmarks.cpp)

target_link_libraries(benchmarks
        ${CONAN_LIBS_GTEST}
        ${CMAKE_THREAD_LIBS_INIT})

add_subdirectory(spscFixed)
add_subdirectory(mpscFixed)
add_subdirectory(spscVariable)
add_subdirectory(mpscVariable)