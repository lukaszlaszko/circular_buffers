#include <cpu_monitor.hpp>

#include <boost/concurrency/spsc_fixed_circular_buffer.hpp>
#include <boost/concurrency/current_thread.hpp>
#include <boost/utils/random.hpp>
#include <boost/utils/timestamp.hpp>

#include <boost/program_options.hpp>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;
using namespace boost::utils;
using namespace boost::concurrency;

static constexpr auto HELP_PARAMETER = "help,h";
static constexpr auto BUFFER_SIZE_PARAMETER = "buffer-size,b";
static constexpr auto BURST_SIZE_PARAMETER = "burst-size,i";
static constexpr auto PRODUCER_CORE_PARAMETER = "producer-core,p";
static constexpr auto CONSUMER_CORE_PARAMETER = "consumer-core,c";

static constexpr auto DEFAULT_BUFFER_SIZE = 1024;
static constexpr auto DEFAULT_BURST_SIZE = 100000000;

static constexpr auto SUCCESS = 0;
static constexpr auto ERROR_IN_COMMAND_LINE = -1;
static constexpr auto ERROR_UNHANDLED_EXCEPTION = -2;


void run(
        bool warmup,
        size_t buffer_size,
        size_t burst,
        size_t producer_core,
        size_t consumer_core)
{
    uint64_t start;
    uint64_t stop;

    spsc_fixed_circular_buffer<uint64_t> buffer(buffer_size);
    thread consumer([&stop, &buffer, consumer_core, burst]()
    {
        current_thread::affinity_mask mask;
        mask.set(consumer_core);
        current_thread::set_affinity(mask);
        
        cpu_monitor monitor;

        auto count = 0ul;
        while (count < burst)
        {
            buffer.consume([&count](const uint64_t& element)
            {
                count++;
                return true;
            });
        }

        stop = timestamp::tick_count();
        monitor.read(cout);
    });

    thread producer([&start, &buffer, producer_core, burst]()
    {
        current_thread::affinity_mask mask;
        mask.set(producer_core);
        current_thread::set_affinity(mask);

        start = timestamp::tick_count();
        for (auto i = 0ul; i < burst; i++)
        {
            while(!buffer.push([i](uint64_t& element)
            {
                element = i;
            }));
        }
    });

    consumer.join();
    producer.join();

    if (!warmup)
    {
        cout << endl;
        cout << "Number of cycles: " << stop - start << endl;
        cout << "Data in bytes: " << sizeof(uint64_t) * burst << endl;
        cout << "Cycles/Block: " << (stop - start) / burst << endl;
    }
}

int main(int argc, char** argv)
{
    try
    {
        bool help;
        size_t buffer_size;
        size_t burst_size;
        size_t producer_core;
        size_t consumer_core;

        boost::program_options::options_description description("Options");
        description.add_options()
            (HELP_PARAMETER, boost::program_options::bool_switch(&help), "Print help messages")
            (BUFFER_SIZE_PARAMETER, boost::program_options::value<size_t>(&buffer_size)->default_value(DEFAULT_BUFFER_SIZE), "Ring buffer size. It's always rounded to the closer multiplication of page size.")
            (BURST_SIZE_PARAMETER, boost::program_options::value<size_t>(&burst_size)->default_value(DEFAULT_BURST_SIZE), "Number of iterations")
            (PRODUCER_CORE_PARAMETER, boost::program_options::value<size_t>(&producer_core)->required(), "Producer thread core id (0-number of cores in the system)")
            (CONSUMER_CORE_PARAMETER, boost::program_options::value<size_t>(&consumer_core)->required(), "Consumer thread core id (0-number of cores in the system)");

        boost::program_options::variables_map variables;
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_command_line(argc, argv, description),
                    variables);

            if (help)
            {
                cout << "SPSC Benchmark:" << endl;
                cout << description << endl;

                return SUCCESS;
            }

            boost::program_options::notify(variables);
        }
        catch (boost::program_options::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << description << endl;

            return ERROR_IN_COMMAND_LINE;
        }

        cout << "Warmup..." << flush;
        run(true, buffer_size, burst_size, producer_core, consumer_core);
        cout << "done" << endl;

        cout << "Running..." << flush;
        run(false, buffer_size, burst_size, producer_core, consumer_core);
        cout << "done" << endl;

        return SUCCESS;
    }
    catch (exception& e)
    {
        cerr << "Unhandled Exception reached the top of main: ";
        cerr << e.what() << ", application will now exit" << endl;

        return ERROR_UNHANDLED_EXCEPTION;
    }
}