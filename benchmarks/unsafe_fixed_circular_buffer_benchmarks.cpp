#include <gtest/gtest.h>

#include <boost/concurrency/unsafe_fixed_circular_buffer.hpp>
#include <boost/utils/statistics.hpp>
#include <boost/utils/timestamp.hpp>

#include <cstdint>
#include <iostream>
#include <vector>

   
using namespace std;
using namespace boost::utils;
using namespace boost::concurrency;

static const uint64_t LARGE_SAMPLE_COUNT = 1000000;


struct Element
{
    uint64_t before;
    uint64_t after;
};

class BenchmarkMixin
{    
protected:
    void after_created(size_t& validStart, size_t& validEnd)
    {
    }
    
    inline void before_push(size_t validStart, size_t validEnd)
    {
    }
    
    inline void after_push(size_t validStart, size_t validEnd, Element* pushed)
    {
        if (pushed == nullptr)
            return;
        
        pushed->after = timestamp::tick_count();
    }
    
    inline void before_consume(size_t validStart, size_t validEnd)
    {
    }
    
    inline void after_consume(size_t validStart, size_t validEnd)
    {
    }
};

TEST(CircularBuffer, PushLetency)
{
    unsafe_fixed_circular_buffer<Element, BenchmarkMixin> buffer(LARGE_SAMPLE_COUNT);
    
    auto start = timestamp::tick_count();
    for (auto i = 0; i < LARGE_SAMPLE_COUNT - 1; i++)
    {
        auto timestamp = timestamp::tick_count();
        buffer.push([timestamp](Element& element) 
        { 
            element.before = timestamp; 
            element.after = timestamp::tick_count();
        });
    }
    auto end = timestamp::tick_count();
    
    vector<uint64_t> scores; 
    scores.reserve(LARGE_SAMPLE_COUNT);
    
    while (buffer.consume([&scores](const Element& element)
    {
        auto diff = element.after - element.before;
        scores.push_back(diff);
    }));
    
    
    cout << "Total push:" << end - start << endl;
    cout << "Single push: " << (end - start) / (LARGE_SAMPLE_COUNT - 1) << endl;
    cout << "Samples: " << (LARGE_SAMPLE_COUNT - 1) << endl;
    
    cout << "Percentiles:" << endl;
    cout << "50 %: " << statistics::quantile(scores, 0.5) << endl;
    cout << "70 %: " << statistics::quantile(scores, 0.7) << endl;
    cout << "90 %: " << statistics::quantile(scores, 0.9) << endl;
    cout << "95 %: " << statistics::quantile(scores, 0.95) << endl;
}

TEST(CircularBuffer, PushConsumeLetency)
{
    unsafe_fixed_circular_buffer<Element, BenchmarkMixin> buffer(LARGE_SAMPLE_COUNT);
    
    auto start = timestamp::tick_count();
    for (auto i = 0; i < LARGE_SAMPLE_COUNT - 1; i++)
    {
        auto timestamp = timestamp::tick_count();
        buffer.push([timestamp](Element& element) { element.before = timestamp; });
    }
    auto end = timestamp::tick_count();
    
    vector<uint64_t> scores; 
    scores.reserve(LARGE_SAMPLE_COUNT);
    
    while (buffer.consume([&scores](const Element& element)
    {
        auto diff = element.after - element.before;
        scores.push_back(diff);
    }));
    
    
    cout << "Total push:" << end - start << endl;
    cout << "Single push: " << (end - start) / (LARGE_SAMPLE_COUNT - 1) << endl;
    cout << "Samples: " << (LARGE_SAMPLE_COUNT - 1) << endl;
    
    cout << "Percentiles:" << endl;
    cout << "50 %: " << statistics::quantile(scores, 0.5) << endl;
    cout << "70 %: " << statistics::quantile(scores, 0.7) << endl;
    cout << "90 %: " << statistics::quantile(scores, 0.9) << endl;
    cout << "95 %: " << statistics::quantile(scores, 0.95) << endl;
}
