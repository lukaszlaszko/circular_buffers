#include <gtest/gtest.h>

#include <boost/concurrency/spsc_fixed_circular_buffer.hpp>
#include <boost/utils/statistics.hpp>
#include <boost/utils/timestamp.hpp>

#include <atomic>
#include <iostream>
#include <thread>
#include <vector>

#include <cstdint>

using namespace std;
using namespace boost::utils;
using namespace boost::concurrency;

static const uint64_t BUFFER_SIZE = 8192;
static const uint64_t LARGE_SAMPLE_COUNT = 1000000;


TEST(spsc_fixed_circular_buffer, PushConsumeLetency)
{
    struct Element
    {
        size_t id;
    };

    spsc_fixed_circular_buffer<Element> buffer(BUFFER_SIZE);
    uint64_t start = 0;
    uint64_t end = 0;
    
    thread consumer([&buffer, &end]()
    {
        uint64_t count = 0;
        while (count < LARGE_SAMPLE_COUNT)
        {
            buffer.consume([&count, &buffer](const Element& element)
            {
                if (element.id != count++)
                    throw "element.id != count++";
                
                return true;
            });
        }
        end = timestamp::tick_count();
        
    });
    
    thread producer([&buffer, &start]()
    {
        start = timestamp::tick_count();
        for (uint64_t i = 0; i < LARGE_SAMPLE_COUNT; i++)
        {
            while (!buffer.push([i](Element& element)
            {
                element.id = i;
            }));
        }
    });
    
    producer.join();
    consumer.join();
    
    cout << "Total consume: " << end - start << endl;
    cout << "Single consume: " << (end - start) / LARGE_SAMPLE_COUNT << endl;
    cout << "Count:" << LARGE_SAMPLE_COUNT << endl;
}

TEST(spsc_fixed_circular_buffer, PushConsumeLetency_Detailed)
{
    struct Element
    {
        size_t id;
        uint64_t pushTimestamp;
    };
    
    spsc_fixed_circular_buffer<Element> buffer(BUFFER_SIZE);
    uint64_t start = 0;
    uint64_t end = 0;
    
    thread producer([&buffer, &start]()
    {
        start = timestamp::tick_count();
        thread_fence();
        for (uint64_t i = 0; i < LARGE_SAMPLE_COUNT; i++)
        {
            uint64_t timestamp = timestamp::tick_count();
            while (!buffer.push([i, timestamp](Element& element)
                                {
                                    element.id = i;
                                    element.pushTimestamp = timestamp;
                                }));
        }
    });
    
    vector<uint64_t> consumed;
    consumed.reserve(LARGE_SAMPLE_COUNT);
    
    thread consumer([&buffer, &end, &consumed]()
    {
        uint64_t count = 0;
        while (count < LARGE_SAMPLE_COUNT)
        {
            buffer.consume([&count, &consumed](const Element& element)
                           {
                                if (element.id != count++)
                                    throw "element.id != count++";

                                consumed.push_back(timestamp::tick_count() - element.pushTimestamp);
                                return true;
                           });
        }
        thread_fence();
        end = timestamp::tick_count();
    });
    
    producer.join();
    consumer.join();
    
    cout << "Total consume: " << end - start << endl;
    cout << "Single consume: " << (end - start) / LARGE_SAMPLE_COUNT << endl;
    cout << "Count:" << LARGE_SAMPLE_COUNT << endl;
    
    cout << "30 %: " << statistics::quantile(consumed, 0.3) << endl;
    cout << "40 %: " << statistics::quantile(consumed, 0.4) << endl;
    cout << "50 %: " << statistics::quantile(consumed, 0.5) << endl;
    cout << "70 %: " << statistics::quantile(consumed, 0.7) << endl;
    cout << "90 %: " << statistics::quantile(consumed, 0.9) << endl;
    cout << "95 %: " << statistics::quantile(consumed, 0.95) << endl;
}
