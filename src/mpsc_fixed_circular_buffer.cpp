#include <boost/concurrency/mpsc_fixed_circular_buffer.hpp>


namespace boost { namespace concurrency {
    
using MpscFixedCircularBuffer_uint8_t = mpsc_fixed_circular_buffer<uint8_t>; 
    
} }
