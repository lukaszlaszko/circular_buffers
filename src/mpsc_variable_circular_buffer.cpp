#include <boost/concurrency/mpsc_variable_circular_buffer.hpp>


namespace boost { namespace concurrency {
    
using MpscVariableCircularBuffer__ = mpsc_variable_circular_buffer<>; 
    
} }
