#pragma once


namespace boost { namespace concurrency {

inline stream_buffer::stream_buffer(size_t capacity)
    :
        variable_circular_buffer(capacity)
{

}

template <typename pointer_type>
inline pointer_type stream_buffer::write_pointer()
{
    std::ptrdiff_t current_end = base::valid_end_.read_dirty();
    return reinterpret_cast<pointer_type>(base::first_map_ + current_end);
}

inline size_t stream_buffer::write_available() const
{
    auto current_end = base::valid_end_.read_dirty();
    auto current_start = base::valid_start_.read_fenced();

    return base::distance(current_end, current_start);
}

inline boost::memory::buffer_ref stream_buffer::write_buf()
{
    return { write_pointer<void*>(), write_available() };
}

inline void stream_buffer::write_commit(size_t size)
{
    auto current_end = base::valid_end_.read_dirty();
    auto next_end = (current_end + size) % base::capacity_;
    base::valid_end_.put_fenced(next_end);
}

template <typename pointer_type>
inline const pointer_type stream_buffer::read_pointer() const
{
    std::ptrdiff_t current_start = base::valid_start_.read_dirty();
    return reinterpret_cast<pointer_type>(base::first_map_ + current_start);
}

inline size_t stream_buffer::read_available() const
{
    auto current_end = base::valid_end_.read_fenced();
    auto current_start = base::valid_start_.read_dirty();
    return current_start != current_end ? base::distance(current_start, current_end) : 0ul;
}

inline const boost::memory::buffer_ref stream_buffer::read_buf() const
{
    return { read_pointer<void*>(), read_available() };
}

inline void stream_buffer::read_commit(size_t size)
{
    auto current_start = base::valid_start_.read_dirty();
    auto next_start = (current_start + size) % base::capacity_;
    base::valid_start_.put_fenced(next_start);
}

}}