#pragma once

#include "variable_circular_buffer.hpp"

#include <boost/memory/buffer_ref.hpp>


namespace boost { namespace concurrency {

/**
 * @brief Circular buffer designated for zero copy stream reads / writes.
 */
class stream_buffer : public variable_circular_buffer
{
public:
    /**
     * @brief Allocates stream buffer with given suggested capacity.
     * @details
     * Buffer capacity will be rounded to the closest mulitplication of page size.
     *
     * @param capacity Suggested, minimal capacity.
     */
    stream_buffer(size_t capacity);

    /**
     * @brief Gets pointer to write zone.
     * @tparam pointer_type Pointer type.
     * @return Pointer to write zone.
     */
    template <typename pointer_type>
    pointer_type write_pointer();
    /**
     * @brief Gets space available for writing.
     * @return Number of bytes available for writing.
     */
    size_t write_available() const;
    /**
     * @brief Gets write buffer specification.
     * @return Write buffer specification.
     */
    boost::memory::buffer_ref write_buf();
    /**
     * @brief Advances write pointer by number of written bytes.
     * @param size Number of written bytes.
     */
    void write_commit(size_t size);
    /**
     * @brief Gets pointer to read zone.
     * @tparam pointer_type Pointer type.
     * @return Pointer to read zone.
     */
    template <typename pointer_type>
    const pointer_type read_pointer() const;
    /**
     * @brief Gets number of bytes available for reading.
     * @return Number of bytes available for reading.
     */
    size_t read_available() const;
    /**
     * @brief Gets read buffer specification.
     * @return Read buffer specification.
     */
    const boost::memory::buffer_ref read_buf() const;
    /**
     * @brief Advences read pointer by given number of bytes.
     * @param size Number of bytes to advance read pointer.
     */
    void read_commit(size_t size);

private:
    using base = variable_circular_buffer;

};

} }

#include "stream_buffer.ipp"
