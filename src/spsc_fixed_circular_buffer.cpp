#include <boost/concurrency/spsc_fixed_circular_buffer.hpp>

namespace boost { namespace concurrency {

using SpscFixedCircularBuffer_uint8_t = spsc_fixed_circular_buffer<uint8_t>;

} }