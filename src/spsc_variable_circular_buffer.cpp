#include <boost/concurrency/spsc_variable_circular_buffer.hpp>

namespace boost { namespace concurrency {

using SpscVariableCircularBuffer__ = spsc_variable_circular_buffer<>;

} }