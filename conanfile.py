from conans import ConanFile, CMake


class CircularBuffersConan(ConanFile):
    name = "circular_buffers"
    version = "1.3"
    license = "Boost Software License - Version 1.0"
    author = "Lukasz Laszko lukaszlaszko@gmail.com"
    url = "https://bitbucket.org/lukaszlaszko/circular_buffers"
    description = "The library provides an easy to use implementation of different circular buffers"
    topics = ("c++", "memory", "ring buffers")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = ["src/*"]
    build_requires = "gtest/1.8.1@bincrafters/stable"
    requires = "allocators/1.2@shadow/stable", "boost/1.69.0@conan/stable"

    def package(self):
        self.copy("*.hpp", dst="include", src="src/include")
        self.copy("*.ipp", dst="include", src="src/include")

    def package_info(self):
        self.cpp_info.libs = ["circular_buffers"]
